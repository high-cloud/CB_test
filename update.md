# 2022.1.6 杨耀东
稍微整合了一下。把Builder基类和HouseBuilder整合进来了。
原来刘洋的实现里许多逻辑在pawn里，目前全部改到Builder和playerController里。

简单介绍下目前几个重要的文件夹：
+ wigets：ui相关
+ character： controller和pawn， 目前只有CB_PlayerController（继承自c++类MyPlayerController）是有用的。
+ Blueprints：蓝图文件夹
+ Mout_Civilian：刘洋之前的实现里用到的素材