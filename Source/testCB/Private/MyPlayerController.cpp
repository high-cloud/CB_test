// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"


AMyPlayerController::AMyPlayerController() {
	InputComponent = CreateDefaultSubobject<UInputComponent>(TEXT("inputs"));

	//mouse event
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;

	// get a builder
	//mRoadConstructor = CreateDefaultSubobject<ARoadConstructor>(TEXT("RoadBuilder"));
}

void AMyPlayerController::SetupInputComponent()
{
	UE_LOG(LogTemp, Warning, TEXT("SetUPInput"));
	InputComponent->BindAction(TEXT("MouseZoomIn"), IE_Pressed, this, &AMyPlayerController::MouseZoomIn);
	InputComponent->BindAction(TEXT("MouseZoomOut"), IE_Pressed, this, &AMyPlayerController::MouseZoomOut);
	InputComponent->BindAction(TEXT("MouseMiddleClick"), IE_Pressed, this, &AMyPlayerController::MouseMiddlePressed);
	InputComponent->BindAction(TEXT("MouseLeftClick"), IE_Pressed, this, &AMyPlayerController::MouseLeftPressed);
	InputComponent->BindAction(TEXT("MouseRightClick"), IE_Pressed, this, &AMyPlayerController::MouseRightPressed);
	InputComponent->BindAction(TEXT("MouseMiddleClick"), IE_Released, this, &AMyPlayerController::MouseMiddleReleased);
	InputComponent->BindAxis(TEXT("moveForward/Backward"), this, &AMyPlayerController::MoveForward);
	InputComponent->BindAxis(TEXT("StrafeLeft/Right"), this, &AMyPlayerController::MoveRight);
	InputComponent->BindAxis(TEXT("CameraOrbit"), this, &AMyPlayerController::CameraYaw);
	InputComponent->BindAxis(TEXT("CameraPitch"), this, &AMyPlayerController::CameraPitch);
	InputComponent->BindAxis(TEXT("MouseX"), this, &AMyPlayerController::MouseX);
	InputComponent->BindAxis(TEXT("MouseY"), this, &AMyPlayerController::MouseY);

}

void AMyPlayerController::BeginPlay()
{
	mRoadConstructor = GetWorld()->SpawnActor<ARoadConstructor>(ARoadConstructor::StaticClass());
	mCarGenerator = GetWorld()->SpawnActor<ACB_CarGenerator>(ACB_CarGenerator::StaticClass());
	mCarGenerator->SetRoadManager(mRoadConstructor->GetRoadManager());
	Super::BeginPlay();
}

void AMyPlayerController::SetModeRoadConstruct()
{
	bModeConstructRoad = true;
	bModeGenerateCar = false;
	bModeBuildBuilding=false;
}

void AMyPlayerController::SetModeCarGenerate()
{
	bModeConstructRoad = false;
	bModeGenerateCar = true;
	bModeBuildBuilding=false;
}

void AMyPlayerController::SetModeBuildBuilding()
{
	bModeConstructRoad = false;
	bModeGenerateCar = false;
	bModeBuildBuilding=true;
}

void AMyPlayerController::SetModeNone()
{
	bModeConstructRoad = false;
	bModeGenerateCar = false;
	bModeBuildBuilding=false;
}

AMyPawn* AMyPlayerController::GetMyPawn()
{
	return Cast<AMyPawn>(GetPawn());
}

void AMyPlayerController::MouseZoomIn()
{
	AMyPawn* pMyPawn = GetMyPawn();
	//UE_LOG(LogTemp, Warning, TEXT("MouseZoonIn"));
	if (pMyPawn)
	{
		//UE_LOG(LogTemp, Warning, TEXT("pMyPawnIs"));

		pMyPawn->MouceZoom(-1.0f);
	}
}

void AMyPlayerController::MouseZoomOut()
{
	AMyPawn* pMyPawn = GetMyPawn();

	if (pMyPawn)
	{
		pMyPawn->MouceZoom(1.0f);
	}
}

void AMyPlayerController::MoveForward(float magnitude)
{
	AMyPawn* pMyPawn = GetMyPawn();

	if (pMyPawn)
	{
		pMyPawn->MoveFoward(magnitude);
	}
}

void AMyPlayerController::MoveRight(float magnitude)
{
	AMyPawn* pMyPawn = GetMyPawn();

	if (pMyPawn)
	{
		pMyPawn->MoveRight(magnitude);
	}
}

void AMyPlayerController::CameraPitch(float magnitude)
{
	AMyPawn* pMyPawn = GetMyPawn();

	if (pMyPawn)
	{
		pMyPawn->CameraPitch(magnitude);
	}
}

void AMyPlayerController::CameraYaw(float magnitude)
{
	AMyPawn* pMyPawn = GetMyPawn();

	if (pMyPawn)
	{
		pMyPawn->CameraYaw(magnitude);
	}
}

void AMyPlayerController::MouseMiddlePressed()
{
	bMouseMiddlePressed = true;
}

void AMyPlayerController::MouseMiddleReleased()
{
	bMouseMiddlePressed = false;
}

void AMyPlayerController::MouseLeftPressed_Implementation()
{
	_MouseLeftPressed();
}

void AMyPlayerController::MouseRightPressed_Implementation()
{
	_MouseRightPressed();
}

void AMyPlayerController::_MouseLeftPressed()
{
	if (bModeConstructRoad)
	{
		mRoadConstructor->MouseLeftPressed(this);
	}
	else if(bModeGenerateCar)
	{
		mCarGenerator->MouseLeftPressed(this);
	}
}

void AMyPlayerController::_MouseRightPressed()
{
	if (bModeConstructRoad)
	{
		mRoadConstructor->MouseRightPressed();
	}
}


void AMyPlayerController::MouseX(float magnitude)
{
	if (bMouseMiddlePressed)
	{
		AMyPawn* pMyPawn = GetMyPawn();

		if (pMyPawn)
		{
			pMyPawn->CameraYaw(magnitude);
		}
	}
}

void AMyPlayerController::MouseY(float magnitude)
{
	if (bMouseMiddlePressed)
	{
		AMyPawn* pMyPawn = GetMyPawn();

		if (pMyPawn)
		{
			pMyPawn->CameraPitch(magnitude);
		}
	}
}


