// Fill out your copyright notice in the Description page of Project Settings.


#include "CB_ClickbleActor.h"
#include "Components/SphereComponent.h"

// Sets default values
ACB_ClickbleActor::ACB_ClickbleActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = SphereComponent;

	this->VisualMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	this->VisualMesh->SetupAttachment(RootComponent);
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));

	if (CubeVisualAsset.Succeeded())
	{
		VisualMesh->SetStaticMesh(CubeVisualAsset.Object);
		VisualMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}
}

// Called when the game starts or when spawned
void ACB_ClickbleActor::BeginPlay()
{
	Super::BeginPlay();
	OnClicked.AddDynamic(this, &ACB_ClickbleActor::OnSelected);
}

// Called every frame
void ACB_ClickbleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACB_ClickbleActor::OnSelected(AActor* target, FKey buttonPressed)
{
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Cyan, FString("EEEEEEEEEEEEEEEEE"));
}

