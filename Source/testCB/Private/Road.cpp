// Fill out your copyright notice in the Description page of Project Settings.

#include "Road.h"
#include "CB_Lane.h"

// Sets default values
ARoad::ARoad() : m_start(FVector(0, 0, 0)), m_end(FVector(-1, 0, 0))
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/MRT/Meshes/RoadMeshes/Pavement/MRT_1_Lane_Road"));

	if (CubeVisualAsset.Succeeded())
	{
		pp = CubeVisualAsset.Object;
	}

	m_road_piece_length = GetLengthOfMesh(pp);
	m_road_width = GetWidthOfMesh(pp);
	m_min_length = 3 * m_road_width;
	m_cross_length = m_road_width / 2 / FMath::Tan(FMath::DegreesToRadians(22.5)); // think 45 is min angle between adjacent roads
																				   //UE_LOG(LogTemp, Warning, TEXT("width is %f"), m_width_of_road);
}


// todo：haven't been used yet, maybe will delete in the future
ARoad::ARoad(FVector start, FVector end)
{
	m_start = start;
	m_end = m_end;

	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/MRT/Meshes/RoadMeshes/Pavement/MRT_1_Lane_Road"));

	if (CubeVisualAsset.Succeeded())
	{
		pp = CubeVisualAsset.Object;
	}

	m_road_piece_length = GetLengthOfMesh(pp);
	m_road_width = GetWidthOfMesh(pp);
	m_min_length = 3 * m_road_piece_length;
	m_cross_length = m_road_piece_length / 2 * FMath::Sin(FMath::DegreesToRadians(22.5)); // think 45 is min angle between adjacent roads

	float length = (end - start).Size();
	int num = length / (int)m_road_piece_length; // num mesh should be add
	FVector vec = end - start;
	vec = (vec / length) * m_road_piece_length;

	FVector nor_vec = vec.GetUnsafeNormal();
	float pitch = FMath::RadiansToDegrees(FMath::Asin(nor_vec | FVector::ZAxisVector));
	FVector xy_vec = vec.GetUnsafeNormal2D();
	float yaw = FMath::RadiansToDegrees(FMath::Acos(nor_vec | FVector::XAxisVector));
	yaw = (xy_vec | FVector::YAxisVector) < 0 ? -yaw : yaw;

	for (int i = 0; i < num; ++i)
	{
		UStaticMeshComponent *pMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RoadMesh"));
		pMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		pMesh->SetMobility(EComponentMobility::Movable);
		pMesh->SetupAttachment(RootComponent);
		pMesh->SetStaticMesh(pp);
		pMesh->SetWorldLocation(start + vec * 0.5 + vec * i);
		pMesh->SetWorldRotation(FRotator(pitch, yaw, 0));
		roadMeshs.Add(pMesh);
	}

	if ((num * m_road_piece_length) < length)
	{
		//  need another scaled mesh
		UStaticMeshComponent *pMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RoadMesh"));
		pMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		pMesh->SetMobility(EComponentMobility::Movable);
		pMesh->SetupAttachment(RootComponent);
		pMesh->SetStaticMesh(pp);
		pMesh->SetWorldLocation(start + vec * 0.5 * (length - num * m_road_piece_length) / m_road_piece_length + vec * num);
		pMesh->SetWorldRotation(FRotator(pitch, yaw, 0));
		pMesh->SetRelativeScale3D(FVector((length - num * m_road_piece_length) / m_road_piece_length, 1, 1));
		roadMeshs.Add(pMesh);
	}
}

bool ARoad::genRoad(FVector start, FVector end)
{
	//this->DestroyConstructedComponents();

	clearMesh();
	m_start = start;
	m_end = end;

	if ((m_start - m_end).Size() < m_min_length)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Red, FString::Printf(TEXT("road length too short")));
		this->Destroy();
		return false;
	}

	float length = (end - start).Size() - m_cross_length * 2;
	int num = length / (int)m_road_piece_length; // num mesh should be add
	FVector vec = end - start;

	FVector nor_vec = vec.GetUnsafeNormal();
	vec = nor_vec * m_road_piece_length;
	float pitch = FMath::RadiansToDegrees(FMath::Asin(nor_vec | FVector::ZAxisVector));
	FVector xy_vec = vec.GetUnsafeNormal2D();
	float yaw = FMath::RadiansToDegrees(FMath::Acos(nor_vec | FVector::XAxisVector));
	yaw = (xy_vec | FVector::YAxisVector) < 0 ? -yaw : yaw;

	for (int i = 0; i < num; ++i)
	{
		UStaticMeshComponent *pMesh = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass());
		pMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		pMesh->SetMobility(EComponentMobility::Movable);
		pMesh->SetupAttachment(RootComponent);
		pMesh->SetStaticMesh(pp);
		pMesh->SetWorldLocation(start + nor_vec * m_cross_length + vec * 0.5 + vec * i);
		pMesh->SetWorldRotation(FRotator(pitch, yaw, 0));
		roadMeshs.Add(pMesh);
	}

	if ((num * m_road_piece_length) < length)
	{
		//  need another scaled mesh
		UStaticMeshComponent *pMesh = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass());
		pMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		pMesh->SetMobility(EComponentMobility::Movable);
		pMesh->SetupAttachment(RootComponent);
		pMesh->SetStaticMesh(pp);
		pMesh->SetWorldLocation(start + nor_vec * m_cross_length + vec * 0.5 * (length - num * m_road_piece_length) / m_road_piece_length + vec * num);
		pMesh->SetWorldRotation(FRotator(pitch, yaw, 0));
		pMesh->SetRelativeScale3D(FVector((length - num * m_road_piece_length) / m_road_piece_length, 1, 1));
		roadMeshs.Add(pMesh);
	}

	RegisterAllComponents();

	setupLanes();
	return true;
}

bool ARoad::genRoad(ARoadCross *start, FVector end)
{
	m_start = start->m_position;
	m_end = end;

	bool ret=genRoad(p_startCross->m_position, end);
	if(ret)
	{

	// p_startCross = start;
	// p_startCross->add(this);
	setCross(true, start);
	}
	return ret;
}

bool ARoad::genRoad(FVector start, ARoadCross *end)
{
	m_start = start;
	m_end = end->m_position;
	bool ret=genRoad(start, end->m_position);
	if(ret)
	{

	setCross(false, end);
	}

	return ret;
}

bool ARoad::genRoad(ARoadCross *start, ARoadCross *end)
{
	m_start = start->m_position;
	m_end = end->m_position;

	bool ret=genRoad(p_startCross->m_position, p_endCross->m_position);

	if(ret)
	{
		
	setCross(true, start);
	setCross(false, end);
	// p_startCross = start;
	// p_endCross = end;
	// p_startCross->add(this);
	// p_endCross->add(this);
	}

	return ret;
}

void ARoad::addRoadCross(bool isStartRoadCross)
{
	ARoadCross *pRoadCross = GetWorld()->SpawnActor<ARoadCross>(ARoadCross::StaticClass());

	if (isStartRoadCross)
	{
		p_startCross = pRoadCross;
		pRoadCross->m_position = m_start + FVector(0, 0, 10);
	}
	else
	{
		p_endCross = pRoadCross;
		pRoadCross->m_position = m_end + FVector(0, 0, 10);
	}
	pRoadCross->add(this);
}

// Called when the game starts or when spawned
void ARoad::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARoad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float ARoad::yaw(bool isStartToEnd)
{
	if (isStartToEnd)
	{
		return (m_end - m_start).Rotation().Yaw;
	}
	else
	{
		return (m_start - m_end).Rotation().Yaw;
	}
}

FVector ARoad::direction(bool isStartToEnd)
{
	if (isStartToEnd)
	{
		return (m_end - m_start).GetUnsafeNormal();
	}
	else
	{
		return (m_start - m_end).GetUnsafeNormal();
	}
}

float ARoad::GetWidthOfMesh(UStaticMesh *staticMesh)
{
	return 2 * staticMesh->GetBounds().BoxExtent.Y;
}

float ARoad::GetLengthOfMesh(UStaticMesh *staticMesh)
{
	return 2 * staticMesh->GetBounds().BoxExtent.X;
}

void ARoad::clearMesh()
{
	for (auto mesh : roadMeshs)
	{
		mesh->DestroyComponent();
	}
	roadMeshs.RemoveAllSwap([](UStaticMeshComponent *)
							{ return true; });
}

// if cross is road's startCross
bool ARoad::isStartCross(ARoadCross *cross)
{
	return cross == this->p_startCross;
}
bool ARoad::isEndCross(ARoadCross *cross)
{
	return cross == this->p_endCross;
}

float ARoad::GetRoadLength()
{
	return (m_end - m_start).Size();
}

void ARoad::setCross(bool isStartCross, ARoadCross *cross)
{
	if (isStartCross)
	{
		p_startCross = cross;
		p_startCross->add(this);
	}
	else
	{
		p_endCross = cross;
		p_endCross->add(this);
	}
}

// you should insure that m_start and m_end is already set
void ARoad::setupLanes()
{
	FVector vec = m_end - m_start;
	FVector nor_vec = vec.GetUnsafeNormal();
	FVector right = (FVector::ZAxisVector ^ vec).GetUnsafeNormal();
	float length = vec.Size();
	vec = (vec / length) * m_road_piece_length;

	FVector start=m_start+nor_vec*m_cross_length;// 排除了留给交叉口的部分
	FVector end=m_end-nor_vec*m_cross_length;// 排除了留给交叉口的部分


	// setup start to end lanes
	for (int i = 0; i < m_start_to_end_lanes_number; ++i)
	{
		ACB_CarLane *pLane = GetWorld()->SpawnActor<ACB_CarLane>(ACB_CarLane::StaticClass());
		pLane->setUp(start+right*(i+0.5f)*m_lane_width,start+right*(i+0.5f)*m_lane_width);
		m_start_to_end_lanes.Add(pLane);
	}
	for (int i = 0; i < m_start_to_end_lanes_number-1; ++i)
	{
		m_start_to_end_lanes[i]->setRightLane(m_start_to_end_lanes[i+1]);
	}
	for (int i = 1; i < m_start_to_end_lanes_number; ++i)
	{
		m_start_to_end_lanes[i]->setLeftLane(m_start_to_end_lanes[i-1]);
	}

	// setup end to start lanes
	for (int i = 0; i < m_end_to_start_lanes_number; ++i)
	{
		ACB_CarLane *pLane = GetWorld()->SpawnActor<ACB_CarLane>(ACB_CarLane::StaticClass());
		pLane->setUp(start+right*(i+0.5f)*m_lane_width,start+right*(i+0.5f)*m_lane_width);
		m_end_to_start_lanes.Add(pLane);
	}
	for (int i = 0; i < m_end_to_start_lanes_number-1; ++i)
	{
		m_end_to_start_lanes[i]->setRightLane(m_end_to_start_lanes[i+1]);
	}
	for (int i = 1; i < m_end_to_start_lanes_number; ++i)
	{
		m_end_to_start_lanes[i]->setLeftLane(m_end_to_start_lanes[i-1]);
	}
}
