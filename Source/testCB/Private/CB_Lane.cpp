// Fill out your copyright notice in the Description page of Project Settings.

#include "CB_Lane.h"

// Sets default values
ACB_Lane::ACB_Lane()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACB_Lane::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACB_Lane::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACB_Lane::setUp(FVector start, FVector end)
{
	m_start = start;
	m_end = end;
}

void ACB_Lane::setLeftLane(ACB_Lane *lane)
{
	m_left_lane = lane;
}

void ACB_Lane::setRightLane(ACB_Lane *lane)
{
	m_right_lane = lane;
}

FVector ACB_Lane::getStart()
{
	return m_start;
}
FVector ACB_Lane::getEnd()
{
	return m_end;
}
