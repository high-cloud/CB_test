// Fill out your copyright notice in the Description page of Project Settings.

#include "CB_CarGenerator.h"
#include "CB_Car.h"
#include "CB_RoadManager.h"

// Sets default values
ACB_CarGenerator::ACB_CarGenerator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UClass> CarFinder(TEXT("Blueprint'/Game/Blueprints/MyCB_Car.MyCB_Car_C'"));
	if (CarFinder.Succeeded())
	{
		CarSubClass = CarFinder.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("can;t find MyCB_Car"));
	}
}

// Called when the game starts or when spawned
void ACB_CarGenerator::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACB_CarGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACB_CarGenerator::GenCar(FVector start, FVector end, ARoad *startRoad, ARoad *endRoad)
{
	ACB_Car *p_Car = GetWorld()->SpawnActor<ACB_Car>(CarSubClass);

	p_Car->SetActorLocation(start);

	if(endRoad==startRoad)
	{
		p_Car->AddDestination(end);
		return ;
	}
	TArray<FVector> path=p_road_manager->FindPath(startRoad,endRoad);
	for(FVector position:path)
	{
		p_Car->AddDestination(position);
	}
	
	if(path.Num()==0)
	{
		return ;
	}
	p_Car->AddDestination(end);
}

void ACB_CarGenerator::MouseLeftPressed(APlayerController *controller)
{
	FHitResult HitResult;
	controller->GetHitResultUnderCursor(ECollisionChannel::ECC_Pawn, false, HitResult);
	UE_LOG(LogTemp, Warning, TEXT("thick is %s"), *HitResult.GetActor()->GetName());

	if (HitResult.GetActor()->GetClass() == ARoad::StaticClass())
	{
		FVector vec = HitResult.Location;
		ARoad *road = Cast<ARoad>(HitResult.GetActor());
		if (b_have_start)
		{
			GenCar(m_start_location, vec, m_start_road, road);
		}
		else
		{
			m_start_location = vec;
			m_start_road=road;
		}

		b_have_start = !b_have_start;
	}
	else
	{
		b_have_start = false;
		UE_LOG(LogTemp, Warning, TEXT("Only support gen car on road now"));
	}
}

void ACB_CarGenerator::SetRoadManager(ACB_RoadManager *RoadManager)
{
	if(!RoadManager)
	{
		UE_LOG(LogTemp, Warning, TEXT("empty pointer to roadManager"));

	}
	p_road_manager=RoadManager;
}
