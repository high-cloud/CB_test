// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawn.h"

// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	RootComponent = SphereComponent;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->SetRelativeRotation(FRotator(330.0f,0.0f,0.0f));
	SpringArm->TargetArmLength = 600.0f;


	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm,USpringArmComponent::SocketName);
	
	FloatingPawnMovment = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("FloatingPawnMovment"));
	InitialWalkSpeed = FloatingPawnMovment->GetMaxSpeed();


}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();


	
}

void AMyPawn::UpdateWalkSpeed()
{
	FloatingPawnMovment->MaxSpeed = SpringArm->TargetArmLength / ZoomMin * InitialWalkSpeed;
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMyPawn::CameraYaw(float magnitude)
{
	FRotator rotator = RootComponent->GetComponentRotation();
	rotator.Yaw = rotator.Yaw + magnitude * RotateSpeed;
	RootComponent->SetWorldRotation(rotator);
}

void AMyPawn::CameraPitch(float magnitude)
{
	FRotator rotator = SpringArm->GetComponentRotation();
	float pitch = rotator.Pitch + magnitude * RotateSpeed;
	pitch = FMath::ClampAngle(pitch, PitchMin, PitchMax);
	rotator.Pitch = pitch;
	rotator.Yaw = 0;
	rotator.Roll = 0;
	SpringArm->SetRelativeRotation(rotator);
}

void AMyPawn::MoveFoward(float magnitude)
{
	FloatingPawnMovment->AddInputVector(this->GetActorForwardVector() * magnitude);
}

void AMyPawn::MoveRight(float magnitude)
{
	FloatingPawnMovment->AddInputVector(this->GetActorRightVector() * magnitude);
}

void AMyPawn::MouceZoom(float magnitude)
{
	SpringArm->TargetArmLength = FMath::Clamp(SpringArm->TargetArmLength + magnitude * ZoomSpeed, ZoomMin, ZoomMax);
	UpdateWalkSpeed();
}

