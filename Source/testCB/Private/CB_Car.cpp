// Fill out your copyright notice in the Description page of Project Settings.

#include "CB_Car.h"

// Sets default values
ACB_Car::ACB_Car()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACB_Car::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACB_Car::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACB_Car::AddDestination(FVector des)
{
	v_destinations.push(des);
}

void ACB_Car::Update(float DeltaTime)
{
	if (v_destinations.empty())
	{
		UE_LOG(LogTemp, Warning, TEXT(" empty fuck"));
		b_needUpdate = true;
		return;
	}

	FVector nextDes = v_destinations.front();
	FVector nowPos = RootComponent->GetRelativeLocation();
	FVector forward = (nextDes - nowPos);
	if (DeltaTime * Speed >= forward.Size())
	{
		RootComponent->SetRelativeLocation(nextDes);
		v_destinations.pop();
	}
	else
	{
		RootComponent->SetRelativeLocation(nowPos + DeltaTime * Speed * forward.GetUnsafeNormal());
	}
}
