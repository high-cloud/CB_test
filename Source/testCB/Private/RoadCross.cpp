// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadCross.h"
#include "Road.h"
#include "CB_Lane.h"
#include <ProceduralMeshComponent/Public/ProceduralMeshComponent.h>

// Sets default values
ARoadCross::ARoadCross()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ARoadCross::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARoadCross::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARoadCross::add(ARoad *pRoad)
{
	// todo lane relate

	if (ifThisIsRoadStart(pRoad))
	{// cross is at start of pRoad

		for (int i = 0; i < roads.Num(); ++i)
		{
			auto road = roads[i];

			// from pRoad to roads
			for (ACB_Lane *start_lane : pRoad->m_end_to_start_lanes)
			{
				if (ifThisIsRoadStart(road))
				{ // cross is at start of road
					for (ACB_Lane *end_lane : road->m_start_to_end_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
				else
				{	// cross is at end of road
					for (ACB_Lane *end_lane : road->m_end_to_start_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
			}


			// from roads to pRoad
			for (ACB_Lane *end_lane : pRoad->m_start_to_end_lanes)
			{
				if (ifThisIsRoadEnd(road))
				{ // cross is at end of road
					for (ACB_Lane *start_lane : road->m_start_to_end_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
				else
				{	// cross is at start of road
					for (ACB_Lane *start_lane : road->m_end_to_start_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
			}
		}
	}
	else
	{// cross is at end of pRoad
		for (int i = 0; i < roads.Num(); ++i)
		{
			auto road = roads[i];

			// from pRoad to roads
			for (ACB_Lane *start_lane : pRoad->m_start_to_end_lanes)
			{
				if (ifThisIsRoadStart(road))
				{ // cross is at start of road
					for (ACB_Lane *end_lane : road->m_start_to_end_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
				else
				{	// cross is at end of road
					for (ACB_Lane *end_lane : road->m_end_to_start_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
			}


			// from roads to pRoad
			for (ACB_Lane *end_lane : pRoad->m_end_to_start_lanes)
			{
				if (ifThisIsRoadEnd(road))
				{ // cross is at start of road
					for (ACB_Lane *start_lane : road->m_start_to_end_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
				else
				{	// cross is at end of road
					for (ACB_Lane *start_lane : road->m_end_to_start_lanes)
					{
						ACB_CrossLane *pLane = GetWorld()->SpawnActor<ACB_CrossLane>(ACB_CrossLane::StaticClass());
						pLane->setUp(start_lane->getEnd(), end_lane->getStart());
						m_lanes_map[std::make_pair(start_lane,end_lane)]=pLane;
					}
				}
			}
		}
	}

	// mesh relate
	for (int i = 0; i < roads.Num(); ++i)
	{
		auto t = roads[i];
		if (pRoad->yaw((ifThisIsRoadStart(pRoad))) > roads[i]->yaw(ifThisIsRoadStart(roads[i])))
		{
			roads.Insert(pRoad, i);
			clearMesh();
			genMesh(); // regenerate mesh
			return;
		}
	}
	roads.Add(pRoad);

	clearMesh();
	genMesh();
}

void ARoadCross::genMesh(FVector position, const TArray<FVector> &directions, const TArray<float> &lengths, const TArray<float> &widths)
{

	UProceduralMeshComponent *pProMesh = NewObject<UProceduralMeshComponent>(this, TEXT("ProceduralMesh"));
	pProMesh->SetupAttachment(RootComponent);
	pProMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;

	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector2D> UVs;

	FVector zup = FVector::ZAxisVector;
	Vertices.Add(position);
	for (int i = 0; i < directions.Num(); ++i)
	{
		FVector left = FVector::CrossProduct(zup, directions[i]);
		Vertices.Add(position + directions[i] * lengths[i] + left * widths[i] / 2);

		FVector right = FVector::CrossProduct(directions[i], zup);
		Vertices.Add(position + directions[i] * lengths[i] + right * widths[i] / 2);
	}

	for (int32 i = 0; i < directions.Num(); ++i)
	{
		Triangles.Add(0);
		Triangles.Add(2 * i + 1);
		Triangles.Add(2 * i + 2);

		Triangles.Add(0);
		Triangles.Add(2 * i + 2);
		Triangles.Add(2 * i + 3);
	}
	Triangles.Pop(false);
	Triangles.Add(1);

	pProMesh->CreateMeshSection(0, Vertices, Triangles, TArray<FVector>(), UVs, TArray<FColor>(), TArray<FProcMeshTangent>(), true);

	RegisterAllComponents();
}

void ARoadCross::genMesh()
{
	TArray<FVector> directions;
	TArray<float> lengths;
	TArray<float> widths;

	for (auto &road : roads)
	{
		directions.Add(road->direction(ifThisIsRoadStart(road)));
		lengths.Add(road->m_cross_length);
		widths.Add(road->m_road_width);
	}

	genMesh(m_position, directions, lengths, widths);
}

void ARoadCross::clearMesh()
{
	UnregisterAllComponents();
	DestroyConstructedComponents();
}

bool ARoadCross::ifThisIsRoadEnd(ARoad *road)
{
	return road->isEndCross(this);
}

bool ARoadCross::ifThisIsRoadStart(ARoad *road)
{
	return road->isStartCross(this);
}

const TArray<ARoad *> &ARoadCross::GetRoads()
{
	return roads;
}
