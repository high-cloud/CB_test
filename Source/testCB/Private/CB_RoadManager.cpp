// Fill out your copyright notice in the Description page of Project Settings.

#include "CB_RoadManager.h"
#include <concurrent_unordered_map.h>
#include <unordered_set>
#include <unordered_map>
#include <list>
#include <algorithm>

// Sets default values
ACB_RoadManager::ACB_RoadManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACB_RoadManager::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACB_RoadManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACB_RoadManager::AddRoad(ARoad *pRoad)
{
	v_roads.Add(pRoad);
}

void ACB_RoadManager::AddCross(ARoadCross *pCross)
{
	v_crosses.Add(pCross);
}

ARoadCross *ACB_RoadManager::GetLastCross()
{
	if (0 != v_crosses.Num())
	{
		return v_crosses.Last();
	}
	return nullptr;
}

TArray<FVector> ACB_RoadManager::FindPath(ARoad *startRoad, ARoad *endRoad)
{
	TArray<FVector> ret;


	ARoadCross* startCross=startRoad->p_startCross;
	ARoadCross* endCross=endRoad->p_endCross;

	ret=FindPath(startCross,endCross);

	// remove specific nodes, 
	if(ret.Num()<2)
	{
		return ret;
	}

	auto sec=ret[1];
	if(ret[ret.Num()-2]==endRoad->p_startCross->m_position)
	{
		ret.Pop();
	}
	if(sec==startRoad->p_endCross->m_position)
	{
		ret.RemoveAt(0);
	}
	return ret;
}

// use A* to find path, actual implementation is from endCross to find startCross
TArray<FVector> ACB_RoadManager::FindPath(ARoadCross *startCross, ARoadCross *endCross)
{
	// if startCross equals endCross, just return this cross's position
	if (startCross == endCross)
	{
		return TArray<FVector>{startCross->m_position};
	}

	std::list<std::pair<float, ARoadCross *>> open_set;
	std::unordered_set<ARoadCross *> close_set;
	std::unordered_map<ARoadCross *, ARoadCross *> parents;

	// f: distance from end to node todo
	// auto f = [](ARoadCross *node)
	// { return 0.0f; };
	
	// h: distance estimated from node to start todo
	auto h = [startCross](ARoadCross *node)
	{ return (node->m_position-startCross->m_position).Size(); };
	// pop a node from open_set, haven't considerd the situation when open_set is empty
	auto popOpenSet = [&open_set,&h]()
	{
		float min=FLT_MAX;
		auto pRet = open_set.begin();
		for (auto now = open_set.begin(); now != open_set.end(); now++)
		{
			if (now->first+ h(now->second)< min)
			{
				pRet = now;
			}
		}
		auto ret = *pRet;
		open_set.erase(pRet);
		return ret;
	};

	// init open_set
	open_set.push_back({0, endCross});

	while (!open_set.empty())
	{
		auto node = popOpenSet();

		// iterate all node's neighbors
		for (auto road : node.second->roads)
		{
			auto neighborCross = road->isStartCross(node.second) ? road->p_endCross : road->p_startCross;

			// if cross is endCross, finish search and return
			if (neighborCross == startCross)
			{
				ARoadCross *nowCross = node.second;
				TArray<FVector> result;
				result.Add(startCross->m_position);
				while (nowCross != endCross)
				{
					result.Add(nowCross->m_position);
					nowCross = parents[nowCross];
				}
				result.Add(endCross->m_position);
				return result;
			}

			// if cross is in close set
			if (close_set.find(neighborCross) != close_set.end())
			{
				continue;
			}

			// iterator point to pair if neighborCross is in openset
			auto it = std::find_if(open_set.begin(), open_set.end(), [neighborCross](std::pair<float, ARoadCross *> &pir)
								   { return pir.second == neighborCross; });
			float length = road->GetRoadLength(); // distance from poped cross to this neighborCross
			if (it == open_set.end())
			{ // this cross hasn't been add to open_set
				open_set.push_back({length + node.first, neighborCross});
				parents[neighborCross]=node.second;
			}
			else
			{ // this cross has been add to open_set
				// if new path is shorter than old, change
				if(length+node.first<it->first)
				{
					it->first=length+node.first;
					parents[neighborCross]=node.second;
				}
			}

			close_set.insert(node.second);
		}
	}

	// no path between startCross and endCross, return a null Tarray
	return TArray<FVector>();
}
