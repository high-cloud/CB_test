// Fill out your copyright notice in the Description page of Project Settings.


#include "RoadConstructor.h"
#include "Road.h"
#include <ProceduralMeshComponent/Public/ProceduralMeshComponent.h>
#include <Runtime/Engine/Classes/Components/SplineMeshComponent.h>


// Sets default values
ARoadConstructor::ARoadConstructor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Spline = CreateDefaultSubobject<USplineComponent>(TEXT("Spline"));
	Spline->SetupAttachment(RootComponent);
	//RootComponent = Spline;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/MRT/Meshes/RoadMeshes/Pavement/MRT_1_Lane_Road"));

	if (CubeVisualAsset.Succeeded())
	{
		pp = CubeVisualAsset.Object;
	}

	

	thick = GetThickness(pp);
	UE_LOG(LogTemp, Warning, TEXT("thick is %f"), thick);
}

// Called when the game starts or when spawned
void ARoadConstructor::BeginPlay()
{
	Super::BeginPlay();
	
	p_roadManager = GetWorld()->SpawnActor<ACB_RoadManager>(ACB_RoadManager::StaticClass());

}

// Called every frame
void ARoadConstructor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// todo: delete this func
void ARoadConstructor::BuildRoad(FVector start, FVector end)
{
	//Spline->AddSplineLocalPoint(start.Location);
	//Spline->AddSplineLocalPoint(end.Location);  
	//USplineMeshComponent* pSplineMesh = NewObject<USplineMeshComponent>(this, USplineMeshComponent::StaticClass());
	//pSplineMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;

	//pSplineMesh->SetMobility(EComponentMobility::Movable);
	//pSplineMesh->SetupAttachment(Spline);
	//pSplineMesh->SetForwardAxis(ESplineMeshAxis::Type::X, false);
	//pSplineMesh->SetStaticMesh(pp);
	//FVector StartPos;
	//FVector StartTangenet;
	//Spline->GetLocationAndTangentAtSplinePoint(0, StartPos, StartTangenet, ESplineCoordinateSpace::Local);
	//FVector EndPos;
	//FVector EndTangnenet;
	//Spline->GetLocationAndTangentAtSplinePoint(1, EndPos, EndTangnenet, ESplineCoordinateSpace::Local);
	//pSplineMesh->SetStartAndEnd(StartPos, StartTangenet, EndPos, EndTangnenet);

	float length = (end - start).Size();
	int num = length / (int)thick; // num mesh should be add
	FVector vec = end - start;
	vec = (vec / length) * thick;

	FVector nor_vec = vec.GetUnsafeNormal();
	float pitch = FMath::RadiansToDegrees( FMath::Asin( nor_vec | FVector::ZAxisVector));
	FVector xy_vec = vec.GetUnsafeNormal2D();
	float yaw = FMath::RadiansToDegrees(FMath::Acos(nor_vec | FVector::XAxisVector));
	yaw = (xy_vec | FVector::YAxisVector) < 0 ? -yaw : yaw;

	for (int i = 0; i < num; ++i)
	{
		UStaticMeshComponent* pMesh = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass());
		pMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		pMesh->SetMobility(EComponentMobility::Movable);
		pMesh->SetupAttachment(RootComponent);
		pMesh->SetStaticMesh(pp);
		pMesh->SetWorldLocation(start+vec*0.5+vec*i);
		pMesh->SetWorldRotation(FRotator(pitch, yaw, 0));
	}

	if ((num * thick) < length)
	{
		//  need another scaled mesh
		UStaticMeshComponent* pMesh = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass());
		pMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		pMesh->SetMobility(EComponentMobility::Movable);
		pMesh->SetupAttachment(RootComponent);
		pMesh->SetStaticMesh(pp);
		pMesh->SetWorldLocation(start + vec * 0.5* (length - num * thick) / thick + vec * num);
		pMesh->SetWorldRotation(FRotator(pitch, yaw, 0));
		pMesh->SetRelativeScale3D(FVector((length - num * thick) / thick, 1, 1));
	}

	RegisterAllComponents();

}   

void ARoadConstructor::MouseLeftPressed(APlayerController* controller)
{
	FHitResult HitResult;
	controller->GetHitResultUnderCursor(ECollisionChannel::ECC_Pawn, false, HitResult);

	UE_LOG(LogTemp, Warning, TEXT("thick is %s"), *HitResult.GetActor()->GetName());

	

	if (!bRoadHalf)
	{	//if it's first time to press, create a start of road
		mStart = HitResult;

		bRoadHalf = true;
	}
	else
	{	// is not first time to press

		ARoad* Road = GetWorld()->SpawnActor<ARoad>(ARoad::StaticClass());

		// if start is a RoadCross
		if (mStart.GetActor()->GetClass() == ARoadCross::StaticClass())
		{
			// if end is also a cross
			if (HitResult.GetActor()->GetClass() == ARoadCross::StaticClass())
			{
				// if gen road success
				if (Road->genRoad(Cast<ARoadCross>(mStart.GetActor()), Cast<ARoadCross>(HitResult.GetActor())))
				{
					mStart = HitResult;
					bNotFirstRoad = true;
					//roads.Add(Road);
					p_roadManager->AddRoad(Road);
				}
				else
				{
					bRoadHalf = false;
					bNotFirstRoad = false;
				}
			}
			else
			{// end is not a cross
				if (Road->genRoad(Cast<ARoadCross>(mStart.GetActor()), HitResult.Location))
				{
					mStart = HitResult;
					bNotFirstRoad = true;
					Road->addRoadCross(false);//generate a cross at end of road
					//roads.Add(Road);
					p_roadManager->AddRoad(Road);
					p_roadManager->AddCross(Road->p_endCross);
					//roadsCrossess.Add(Road->p_endCross);
				}
				else
				{
					bRoadHalf = false;
					bNotFirstRoad = false;
				}
			}
		}
		else
		{// start is not a cross
			if (HitResult.GetActor()->GetClass() == ARoadCross::StaticClass())
			{
				if (Road->genRoad(mStart.Location, Cast<ARoadCross>(HitResult.GetActor())))
				{// end is a cross
					if (bNotFirstRoad)
					{// is continuing construct road, so you should get last cross as start
						//Road->p_startCross = roadsCrossess.Last();
						Road->setCross(true,p_roadManager->GetLastCross());
						// Road->p_startCross = p_roadManager->GetLastCross();
						// Road->p_startCross->add(Road);
					}
					else
					{
						Road->addRoadCross(true);
						p_roadManager->AddCross(Road->p_startCross);
						//roadsCrossess.Add(Road->p_startCross);
					}
					mStart = HitResult;
					bNotFirstRoad = true;
					p_roadManager->AddRoad(Road);
					//roads.Add(Road);
				}
				else
				{// start and end both are not a cross
					bRoadHalf = false;
					bNotFirstRoad = false;
				}
			}
			else
			{
				if (Road->genRoad(mStart.Location, HitResult.Location))
				{
					if (bNotFirstRoad)
					{
						Road->setCross(true,p_roadManager->GetLastCross());
						// Road->p_startCross = p_roadManager->GetLastCross();
						//Road->p_startCross = roadsCrossess.Last();
						// Road->p_startCross->add(Road);
					}
					else
					{
						Road->addRoadCross(true);
						p_roadManager->AddCross(Road->p_startCross);
						//roadsCrossess.Add(Road->p_startCross);
					}
					mStart = HitResult;
					bNotFirstRoad = true;
					//roads.Add(Road);
					p_roadManager->AddRoad(Road);
					Road->addRoadCross(false);
					//roadsCrossess.Add(Road->p_endCross);
					p_roadManager->AddCross(Road->p_endCross);
				}
				else
				{
					bRoadHalf = false;
					bNotFirstRoad = false;
				}
			}



		}



	}
}

void ARoadConstructor::MouseRightPressed()
{
	bRoadHalf = false;
	bNotFirstRoad = false;
}

float ARoadConstructor::GetThickness(UStaticMesh* staticMesh)
{
	return 2 * staticMesh->GetBounds().BoxExtent.X;
}

void ARoadConstructor::test(FVector location)
{
	TArray<FVector> directions;
	TArray<float> lengths;
	TArray<float> widths;

	directions.Add(FVector(0.5, 0.87, 0));
	directions.Add(FVector(0.5, -0.87, 0));

	lengths.Add(100);
	lengths.Add(100);

	widths.Add(60);
	widths.Add(60);

	genCrossing(FVector(0, 0, 200), directions, lengths, widths);

}   

void ARoadConstructor::genCrossing(FVector position, TArray<FVector> directions,TArray<float> lengths, TArray<float> widths)
{

	UProceduralMeshComponent* pProMesh = NewObject<UProceduralMeshComponent>(this, TEXT("ProceduralMesh"));
	pProMesh->SetupAttachment(RootComponent);
	pProMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;

	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector2D> UVs;

	FVector zup = FVector::ZAxisVector;
	Vertices.Add(position);
	for (int i=0;i<directions.Num();++i)
	{
		FVector left = FVector::CrossProduct(zup,directions[i]);
		Vertices.Add(position + directions[i] * lengths[i] + left * widths[i]);

		FVector right = FVector::CrossProduct(directions[i], zup);
		Vertices.Add(position + directions[i] * lengths[i] + right * widths[i]);
	}

	for (int32 i = 0; i < directions.Num(); ++i)
	{
		Triangles.Add(0);
		Triangles.Add(2 * i + 1);
		Triangles.Add(2 * i + 2);

		Triangles.Add(0);
		Triangles.Add(2 * i + 2);
		Triangles.Add(2 * i + 3);
	}
	Triangles.Pop(false);
	Triangles.Add(1);

	pProMesh->CreateMeshSection(0, Vertices, Triangles, TArray<FVector>(), UVs, TArray<FColor>(), TArray<FProcMeshTangent>(), true);

	RegisterAllComponents();
}

