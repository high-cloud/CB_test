// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "MyPawn.generated.h"





UCLASS()
class TESTCB_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawn();

	// vars
	UPROPERTY(VisibleAnywhere)
		float RotateSpeed=1.0f;
	UPROPERTY(VisibleAnywhere)
		float ZoomSpeed=1000.0f;
	UPROPERTY(VisibleAnywhere)
		float ZoomMin=100.0f;
	UPROPERTY(VisibleAnywhere)
		float ZoomMax=1000000.0f;
	UPROPERTY(VisibleAnywhere)
		float InitialWalkSpeed=100.0f;
	UPROPERTY(VisibleAnywhere)
		float PitchMax=350.0f;
	UPROPERTY(VisibleAnywhere)
		float PitchMin=300.f;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void UpdateWalkSpeed();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//---------camera relate---------------
	void CameraYaw(float magnitude);
	void CameraPitch(float magnitude);
	void MouceZoom(float magnitude);
	void MoveFoward(float magnitude);
	void MoveRight(float magnitude);


private:
	//-------------components----------------
	UPROPERTY(VisibleAnywhere)
		USphereComponent* SphereComponent;

	UPROPERTY(VisibleAnywhere)
		USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere)
		UFloatingPawnMovement* FloatingPawnMovment;
		

};
