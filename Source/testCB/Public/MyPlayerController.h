// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPawn.h"
#include "RoadConstructor.h"
#include "CB_CarGenerator.h"

#include "MyPlayerController.generated.h"

UCLASS()
class TESTCB_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMyPlayerController();

	bool bMouseMiddlePressed = false;

	UPROPERTY(BlueprintReadWrite, Category = "Mode")
	bool bModeConstructRoad = false;
	UPROPERTY(BlueprintReadWrite, Category = "Mode")

	bool bModeGenerateCar = false;
	UPROPERTY(BlueprintReadWrite, Category = "Mode")

	bool bModeBuildBuilding = false;

	ARoadConstructor *mRoadConstructor;
	ACB_CarGenerator *mCarGenerator;

protected:
	virtual void SetupInputComponent() override;

	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable)
	void SetModeRoadConstruct();

	UFUNCTION(BlueprintCallable)
	void SetModeCarGenerate();

	UFUNCTION(BlueprintCallable)
	void SetModeBuildBuilding();

	UFUNCTION(BlueprintCallable)
	void SetModeNone();

	AMyPawn *GetMyPawn();
	void MouseZoomIn();
	void MouseZoomOut();
	void MoveForward(float magnitude);
	void MoveRight(float magnitude);
	void CameraPitch(float magnitude);
	void CameraYaw(float magnitude);
	void MouseMiddlePressed();
	void MouseMiddleReleased();

	UFUNCTION(BlueprintNativeEvent)
	void MouseLeftPressed();

	UFUNCTION(BlueprintNativeEvent)
	void MouseRightPressed();

	// real implementation for MouseLeftPressed
	UFUNCTION(BlueprintCallable)
	void _MouseLeftPressed(); 

	UFUNCTION(BlueprintCallable)
	void _MouseRightPressed(); // real implementation for MouseRightPressed


	void MouseX(float magnitude);
	void MouseY(float magnitude);
};
