// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Runtime/Engine/Classes/Components/SplineComponent.h>
#include "RoadCross.h"
#include "CB_CarLane.h"



#include "Road.generated.h"

UCLASS()
class TESTCB_API ARoad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoad();

	USplineComponent* Spline;
	UStaticMesh* pp; // a pointer to mesh
	float m_road_piece_length;
	float m_road_width;
	float m_min_length;
	float m_cross_length; // length reserved for cross mesh

	TArray<UStaticMeshComponent*> roadMeshs;
	ARoadCross* p_startCross;
	ARoadCross* p_endCross;
	FVector m_start;
	FVector m_end;
	
	// lane relate
	uint8 m_start_to_end_lanes_number=1;
	uint8 m_end_to_start_lanes_number=1;
	TArray<ACB_CarLane*> m_start_to_end_lanes;
	TArray<ACB_CarLane*> m_end_to_start_lanes;
	float m_lane_width;


	ARoad(FVector start,FVector end);
	bool genRoad(FVector start, FVector end);
	bool genRoad(ARoadCross* start, FVector end);
	bool genRoad(FVector start, ARoadCross* end);
	bool genRoad(ARoadCross* start,ARoadCross* end);
	void addRoadCross(bool isStartRoadCross);
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	float yaw(bool isStartToEnd);
	FVector direction(bool isStartToEnd); // return normalied vector from start/end
	static float GetLengthOfMesh(UStaticMesh* staticMesh); // return thick ness of mesh to split road into many parts
	static float GetWidthOfMesh(UStaticMesh* staticMesh); // return width ness of mesh to split road into many parts
	void clearMesh();

	bool isStartCross(ARoadCross* cross);
	bool isEndCross(ARoadCross* cross);
	float GetRoadLength();


	void setCross(bool isStartCross,ARoadCross* cross);

	void setupLanes();
};
