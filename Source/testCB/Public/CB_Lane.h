// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"




#include "CB_Lane.generated.h"


class ACB_Car;

class ARoad;
UCLASS()
class TESTCB_API ACB_Lane : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACB_Lane();


private:
	// ARoad* p_road;

	FVector m_start;
	FVector m_end;



	TArray<ACB_Car*> car_list;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	ACB_Lane* m_right_lane=nullptr;
	ACB_Lane* m_left_lane=nullptr;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	void setUp(FVector start,FVector end); 
	void setLeftLane(ACB_Lane*);
	void setRightLane(ACB_Lane*);

	FVector getStart();
	FVector getEnd();
};
