// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CB_ClickbleActor.generated.h"

UCLASS()
class TESTCB_API ACB_ClickbleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACB_ClickbleActor();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* VisualMesh;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnSelected(AActor* target, FKey buttonPressed);
};
