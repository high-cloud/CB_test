// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CB_Lane.h"
#include "CB_CrossLane.h"
#include <utility>
#include <map>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RoadCross.generated.h"

class ARoad;

UCLASS()
class TESTCB_API ARoadCross : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoadCross();

	TArray<ARoad*> roads; // roads linked to this crossing

	FVector m_position;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	std::map<std::pair<ACB_Lane*,ACB_Lane*>,ACB_CrossLane*> m_lanes_map;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void add(ARoad* pRoad);
	void genMesh(FVector position, const TArray<FVector>& directions, const TArray<float>& lengths, const TArray<float>& widths);
	void genMesh(); // a version gen m meshes with roads already added

	void clearMesh();
	bool ifThisIsRoadEnd(ARoad* road);
	bool ifThisIsRoadStart(ARoad* road);

	const TArray<ARoad*> & GetRoads();
};
