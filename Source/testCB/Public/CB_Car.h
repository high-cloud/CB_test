// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <queue>
#include "GameFramework/Actor.h"







#include "CB_Car.generated.h"

UCLASS()
class TESTCB_API ACB_Car : public AActor
{
	GENERATED_BODY()
	

private:
	std::queue<FVector> v_destinations;
	bool b_needUpdate = false;

	
public:	
	// Sets default values for this actor's properties
	ACB_Car();

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category="Speed")
	float Speed = 100;  // cm/s
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddDestination(FVector des);

	UFUNCTION(BlueprintCallable)
	void Update(float DeltaTime);

};
