// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Road.h"
#include"CB_Car.h"
#include"CB_RoadManager.h"





#include "CB_CarGenerator.generated.h"

UCLASS()
class TESTCB_API ACB_CarGenerator : public AActor
{
	GENERATED_BODY()
	
	bool b_have_start=false;
	FVector m_start_location;
	ARoad* m_start_road;
public:	
	// Sets default values for this actor's properties
	ACB_CarGenerator();

	UPROPERTY(BlueprintReadWrite)
	TSubclassOf<ACB_Car> CarSubClass;

	ACB_RoadManager* p_road_manager;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void GenCar(FVector start, FVector end, ARoad* startRoad, ARoad* endRoad);
	void MouseLeftPressed(APlayerController*);
	void SetRoadManager(ACB_RoadManager* RoadManager);
};
