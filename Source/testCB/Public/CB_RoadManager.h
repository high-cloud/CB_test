// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Road.h"
#include "RoadCross.h"






#include "CB_RoadManager.generated.h"

UCLASS()
class TESTCB_API ACB_RoadManager : public AActor
{
	GENERATED_BODY()
	

private:
	TArray<ARoad*> v_roads;
	TArray<ARoadCross*> v_crosses;
public:	
	// Sets default values for this actor's properties
	ACB_RoadManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddRoad(ARoad* pRoad);
	void AddCross(ARoadCross* pCross);
	ARoadCross* GetLastCross();
	TArray<FVector> FindPath(ARoad* startRoad, ARoad* endRoad);
	TArray<FVector> FindPath(ARoadCross* startCross, ARoadCross* endCross);

};
