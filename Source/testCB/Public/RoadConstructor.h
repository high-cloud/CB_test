// Fill out your copyright notice in the Description page of Project Settings.

#pragma once







#include "CB_RoadManager.h"
#include "Road.h"
#include "RoadCross.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Runtime/Engine/Classes/Components/SplineComponent.h>
#include "RoadConstructor.generated.h"
UCLASS()
class TESTCB_API ARoadConstructor : public AActor
{
	GENERATED_BODY()
	
	bool bRoadHalf = false; // selected start, haven't select end point of road
	bool bNotFirstRoad = false;
	FHitResult mStart;
	USplineComponent* Spline;
	UStaticMesh* pp; // mesh to add to pp
	float thick;
	
	ACB_RoadManager* p_roadManager;
	TArray<ARoad*> roads;
	TArray<ARoadCross*> roadsCrossess;

public:	
	// Sets default values for this actor's properties
	ARoadConstructor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void BuildRoad(FVector start, FVector end);
	void MouseLeftPressed(APlayerController*);
	void MouseRightPressed();
	static float GetThickness(UStaticMesh* staticMesh); // return thickness of mesh to split road into many parts

	void test(FVector location);
	void genCrossing(FVector position, TArray<FVector> directions, TArray<float> lengths, TArray<float> widths);
	ACB_RoadManager* GetRoadManager(){return p_roadManager;}

};
